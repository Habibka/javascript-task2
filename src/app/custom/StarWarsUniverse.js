import Entity from './Entity';

export default class StarWarsUniverse {
  constructor() {
    this.entities = [];
  }
  async init() {
    const rootData = await fetch('https://swapi.booost.bg/api/').then((response) => response.json());

    for (const key in rootData) {
      const entityData = await fetch(rootData[key]).then((response) => response.json());

      this.entities.push(new Entity(key, entityData));
    }
  }
}
